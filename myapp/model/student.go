package model

import (
	"myapp/datastore/postgres"
)

type Student struct {
	StdId int64 `json:"stdid"`
	FirstName string `json:"fname"`
	LastName string `json:"lname"`  
	Email string `json:"email"`
}

//  constant to store SQL query to insert data into the table in the database
const( 
	queryInsert = "INSERT INTO  student(stdid, firstname, lastname, email) VALUES($1, $2, $3,$4);"
	queryGet = "SELECT stdid, firstname, lastname, email FROM student WHERE stdid = $1;"
	// queryUpdate = "UPDATE student SET stdid=$1, firstname=$2, lastname=$3, email=$4 WHERE stdid=$5;" //for Exec 
	queryUpdate = "UPDATE student SET stdid=$1, firstname=$2, lastname=$3, email=$4 WHERE stdid=$5 RETURNING stdid;" //for query row
	// RETURNING key word returns the whole row or particular row selected  
	queryDeleteUser = "DELETE FROM student WHERE stdid=$1 RETURNING stdid" ///RETURNIGN key word to reurn sth like stdid for now
	)

// Create a receiver function for student type to add student data 
func (s *Student) Create() error{  //
	// fmt.Println(s)
	// exec to insert but doenst want any return anything
	_, err := postgres.DB.Exec(queryInsert, s.StdId, s.FirstName, s.LastName, s.Email) 
	return err 

}

func (s *Student) Read() error {
	row := postgres.DB.QueryRow(queryGet, s.StdId)
	err := row.Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email)
	return err
}
func (s *Student) Update(oldID int64) error {
	// _, err := postgres.DB.Exec(queryUpdate, s.StdId, s.FirstName, s.LastName, s.Email, oldID) // using exec
	err := postgres.DB.QueryRow(queryUpdate, s.StdId, s.FirstName, s.LastName, s.Email, oldID).Scan(&s.StdId) // query row

	return err
}

func (s *Student) Delete() error {
	if err := postgres.DB.QueryRow(queryDeleteUser, s.StdId).Scan(&s.StdId); err != nil {
		return err
	} 
	return nil 
}


func GetAllStudents() ([]Student, error) {
	rows, getErr := postgres.DB.Query("SELECT * from student;")
	if getErr != nil {
		return nil, getErr
	}

	// Create a slice of type student 
	students := []Student{}

	for rows.Next() { // next iterates rows one by one
		var s Student 
		dbErr := rows.Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email)
		if dbErr != nil {
			return nil, dbErr
		}

		students = append(students, s)
	}
	rows.Close() // close() closes the connections with the database
	return students, nil
}