package model

import (
	"myapp/datastore/postgres"

	_ "github.com/lib/pq"
)
type Course struct {
	CourseID string `json:"cid"`
	CourseName string `json:"cname"`
}


const(
	queryInsertCourse = "INSERT INTO course (cid, coursename) VALUES($1, $2);"
	queryGetCourse = "SELECT cid, coursename FROM course WHERE cid=$1 ;"
	queryUpdateCourse = "UPDATE course SET cid=$1, coursename=$2 WHERE cid=$3 RETURNING cid;" //for query row
	queryDeleteCourse = "DELETE FROM course WHERE cid=$1 RETURNING cid" ///RETURNIGN key word to reurn sth like stdid for now

)

func (c *Course) Create() error {
	_,err := postgres.DB.Exec(queryInsertCourse, c.CourseID, c.CourseName);
	return err 
} 


func (c *Course) Read() error {
	row := postgres.DB.QueryRow(queryGetCourse, c.CourseID);
	err := row.Scan(&c.CourseID, &c.CourseName)
	return err
}


func (c *Course) Update(old_cid string) error {
	err := postgres.DB.QueryRow(queryUpdateCourse, c.CourseID, c.CourseName, old_cid).Scan(&c.CourseID)
	return err
}

func (c *Course) Delete() error {
	if err := postgres.DB.QueryRow(queryDeleteCourse, c.CourseID).Scan(&c.CourseID); err != nil {
		return err
	}
	return nil
}


func GetAllCourses() ([]Course, error) {
	rows, getErr := postgres.DB.Query("SELECT * from course;")
	if getErr != nil {
		return nil, getErr
	}

	courses := []Course{}

	for rows.Next() {
		var c Course
		dbErr := rows.Scan(&c.CourseID, &c.CourseName)
		if dbErr != nil {
			return nil, dbErr
		}

		courses = append(courses, c)
	}
	rows.Close()
	return courses, nil
}

// func GetAllStudents() ([]Student, error) {
// 	rows, getErr := postgres.DB.Query("SELECT * from student;")
// 	if getErr != nil {
// 		return nil, getErr
// 	}

// 	// Create a slice of type student 
// 	students := []Student{}

// 	for rows.Next() { // next iterates rows one by one
// 		var s Student 
// 		dbErr := rows.Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email)
// 		if dbErr != nil {
// 			return nil, dbErr
// 		}

// 		students = append(students, s)
// 	}
// 	rows.Close() // close() closes the connections with the database
// 	return students, nil
// }