
window.onload = function() {
    fetch("/courses")
        .then(response => response.text())
        .then(data => showCourses(data))
}



function addCourse(){
    var data = getFormData()

    var cid = data.cid

    fetch('/course', {  // mention only the route defined in backend
    method: "POST", // the method used to write the data
    body: JSON.stringify(data), //convert the js object to json format using stringify from JSON library
    headers: {"Content-type": "application/json; charset=UTF-8"}//request body
    }).then(response1 =>{ //checking response successs or not
        if(response1.ok){
            fetch('/course/'+cid)
            .then(response2 =>response2.text())
            .then(data=>showCourse(data))
        } else {
            //  catching error 
            throw new Error(response1.statusText)
        }
    }).catch(e => {
        alert(e)
    })

    if (cid == "") {
        alert(" Course ID cannot be blank")
        return
        // } else if (data.email == "") {
        // alert("course namw cannot be empty")
    } else if(data.cname == ""){
        alert("Course name cannot be empty")
        return
        }

        resetForm()
}

function showCourse(data) {
    const course = JSON.parse(data)
    newRow(course)

}


// set the form empty
function resetForm() {
    document.getElementById("cid").value = "";
    document.getElementById("cname").value = "";
}


function showCourses(data) {
    const courses = JSON.parse(data)
    courses.forEach(cou => {
        newRow(cou)
    });

}

function newRow(course) {
    // Find a <table> element with id="myTable":
    var table = document.getElementById("myTable");
    // Create an empty <tr> element and add to the last position of the table:
    var row = table.insertRow(table.length);
    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    var td=[]
    for(i=0; i<table.rows[0].cells.length; i++){
        td[i] = row.insertCell(i);
    }
     // Add student detail to the new cells:
     td[0].innerHTML =course.cid;
     td[1].innerHTML =course.cname;
     td[2].innerHTML = '<input type="button" onclick="deleteCourse(this)"value="delete" id="button-1">';
     td[3].innerHTML = '<input type="button" onclick="updateCourse(this)"value="edit" id="button-2">';
}

var selectedRow = null
function updateCourse(r) {
    // extracting the row 
    selectedRow = r.parentElement.parentElement;

    // fill in the form fields with selected row
    document.getElementById("cid").value = selectedRow.cells[0].innerHTML; //innerHTML to access the value to cell 
    document.getElementById("cname").value = selectedRow.cells[1].innerHTML;


    var btn = document.getElementById("button-add");
    cid = selectedRow.cells[0].innerHTML;
    if (btn) {
        btn.innerHTML = "update";
        btn.setAttribute("onclick", "update(cid)");
    }
}

function update(cid) {
    var newData = getFormData()

    fetch('/course/' + cid, {  // mention only the route defined in backend
            method: "PUT", // the method used to write the data
            body: JSON.stringify(newData), //convert the js object to json format using stringify from JSON library
            headers: {"Content-type": "application/json; charset=UTF-8"}//r
    }).then(res => {
        if (res.ok) {
            selectedRow.cells[0].innerHTML = newData.cid;
            selectedRow.cells[1].innerHTML = newData.cname;
            // set the initial values
            var btn = getElementById("button-add");
            if (btn) {
                btn.innerHTML = "Add";
                btn.setAttribute("onclick", "addCourse()");
                selectedRow = null;
                resetForm()

               
            } else {
                alert ("Server: Update request error")
            } 
         }
    })
  
}

function getFormData() {
    var formData = {
        cid : document.getElementById("cid").value,
        cname : document.getElementById("cname").value,
        }
        return formData
}

function deleteCourse(r){
    // this(input) -> td -> tr 
    if (confirm('Are you sure you want to DELETE this record?')){
    selectedRow = r.parentElement.parentElement;
    cid = selectedRow.cells[0].innerHTML;

    fetch('/course/'+cid, {
        method: "DELETE",
        headers: {"Content-type": "application/json; charset=UTF-8"}
    });
    var rowIndex = selectedRow.rowIndex; // index starts from 0
    if (rowIndex>0) { //th is row 0
        document.getElementById("myTable").deleteRow(rowIndex);
    } 
        selectedRow = null;
    }
}