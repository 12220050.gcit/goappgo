function signUp() {

    // data to be sent to the POST request

    var _data = {
        firstname : document.getElementById("fname").value,
        lastname : document.getElementById("lname").value,
        email : document.getElementById("email").value,
        password : document.getElementById("pw1").value,
        pw : document.getElementById("pw2").value
    }

    // check password matches to confirm password
    if (_data.password !== _data.pw)  {
        alert ("PASSWORD doen't match!")
        return
    }

    // send sign up request
    fetch ("/signup", {
        method: "POST",
        body: JSON.stringify(_data), //js object to json object
        headers: {"content-type": "application/json; charset=UTF-8"}
    })
    .then(response => {
        if (response.status == 201) { // 201 is for status created on the server
            window.open("index.html", "_self") //second argument to open in the existing tab not in the new tab
        }
    });
    // else 
}

// function reset() {
    
//     var _data = {
//         firstname : document.getElementById("fname").value = "",
//         lastname : document.getElementById("lname").value = "",
//         email : document.getElementById("email").value = "",
//         password : document.getElementById("pw1").value= "",
//         pw : document.getElementById("pw2").value = ""
//     }
//     return 
// }