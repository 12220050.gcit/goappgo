function login() {

    // data to be sent to the POST request

    var _data = {
        email : document.getElementById("email").value,
        password : document.getElementById("pw").value
    }



    // send sign up request
    fetch ("/login", {
        method: "POST",
        body: JSON.stringify(_data), //js object to json object
        headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(response => {
        if (response.ok) { // 201 is for status created on the server
            window.open("student.html", "_self") //second argument to open in the existing tab not in the new tab
        }
        else {
            throw new Error(response.statusText)
            // alert ("invalid password")
        }
    }).catch(e => {
        if (e == "Error: Unauthorized"){
            alert(e+ ".Credentials does not match!")
            return
        }
    });
  
}

function Logout() {
    fetch('/logout')
    .then(response => {
        if (response.ok) {
            window.open("index.html", "_self")
        } else {
            throw new Error(response.StatusText)
        }
    }) 
    .catch (e => {
        alert(e)
    })
}

