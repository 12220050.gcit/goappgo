package postgres

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

// db info
const (
	postgres_host = "dpg-chq2bnfdvk4goeoqhod0-a.singapore-postgres.render.com"
	postgres_port = 5432
	postgres_user = "postgres_admin"
	postgres_password = "P6pr7a6HUKu7x6jQAarxpRN7NkynflWF"
	postgres_dbname = "my_db_zkbx"
)

// creating a DB variable that points to the DB which is in sql package ... db is struct type
var DB *sql.DB

// init() is always called befroe main()
func init() {
	// TO CREATE STRING CONNECTION
	// converting into a string type using Sprintf ()
	db_info := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s ",postgres_host, postgres_port,  postgres_user, postgres_password, postgres_dbname)

	// to open the connection  to database
	var err error
	DB, err = sql.Open("postgres", db_info) //drivername postgres and data source name
// sql.Open() returns a pointer to a sql.DB and an error
	if err != nil {
		log.Println(err)
	} else{
		log.Println("Database successfully connected")
	}
}