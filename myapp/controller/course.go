package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"

	"github.com/gorilla/mux"
)

func AddCourse (w http.ResponseWriter, r *http.Request) {
	var cou model.Course 
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&cou)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json data")
		return
	}
	dbErr := cou.Create()
	if dbErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message":"course data added"})
}

// func getCourseId(cid string) (string) {
// 	ccid, _ := strconv.ParseInt(cid, 10, 64) 
// 	return ccid 
// }

func GetCourse (w http.ResponseWriter, r *http.Request){
	cid := mux.Vars(r)["cid"]
	cou := model.Course{CourseID:cid}

	getErr := cou.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "course not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, cou)
	}
}

func UpdateCourse(w http.ResponseWriter, r *http.Request) {
	old_cid := mux.Vars(r)["cid"]
	var cou model.Course 
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&cou)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	} 
	updateErr := cou.Update(old_cid)
	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w,http.StatusNotFound, "course not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, cou)
	}
}

func DeleteCourse(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]
	c := model.Course{CourseID: cid} 
	if err := c.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, map[string]string {"status": "deleted"})
	}
}

func GetAllCourses(w http.ResponseWriter, r *http.Request) {
	courses, getErr := model.GetAllCourses()
if getErr != nil {
	httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
	return
}
httpResp.RespondWithJSON(w, http.StatusOK, courses)
}