package controller

import (
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"time"
)

func Signup(w http.ResponseWriter, r *http.Request) {
		var admin model.Admin
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&admin); err != nil {
			httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json body")
			return 
		}

		defer r.Body.Close()
		saveErr := admin.Create()
		if saveErr != nil {
			httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
			return
		}

		//  no error 
		httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "admin added"})
}
var admin model.Admin

func Login(w http.ResponseWriter, r *http.Request) {
	// var admin model.Admin
	// decoder := json.NewDecoder(r.Body)
	err := json.NewDecoder(r.Body).Decode(&admin)
	// if err := decoder.Decode(&admin); err != nil {
	if err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json body")
		return 
	}

	defer r.Body.Close()
	getErr := admin.Get()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
		return
	}
	// craeting cookie
	cookie := http.Cookie{
		Name: "my-cookie",
		Value: "cookie-value",
		Expires: time.Now().Add(20 * time.Minute), // for 20 minutes after which the session expires
		Secure: true,
	}

	// set cookie and send back to client 
	http.SetCookie(w, &cookie) 
	//  no error 
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "success"})
}




func Logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{ // create and set the cookie in one line
		Name: "my-cookie",
		Expires: time.Now(),
	})

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "logout success"})
}



func VerifyCookie(w http.ResponseWriter,  r *http.Request)  bool{
	cookie, err := r.Cookie("my-cookie")
	if err != nil {
		if err == http.ErrNoCookie {
			httpResp.RespondWithError(w, http.StatusSeeOther, "Cookie not set")
			return false
		}
		httpResp.RespondWithError(w, http.StatusInternalServerError, "internal server error")
		return false
	} 
	// validate cookie value
	if cookie.Value != "cookie-value" {
		httpResp.RespondWithError(w, http.StatusSeeOther, "Cookie value does not match")
		return false
	}
	
	return true 
}