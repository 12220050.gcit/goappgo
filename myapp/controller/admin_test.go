package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdmLogin(t *testing.T) {
	url := "http://localhost:8080/login"
	var data = []byte(`{"email": "12220050.gcit@rub.edu.bt", "password": "12220050"}`)

	// create a req object 
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))


	// create a req object header
	req.Header.Set("content-Type", "application/json")

	// create a client
	client := &http.Client{}

	// send the post request // we get http respponse and an error 
	resp, err := client.Do(req) // do send http request to the server and pass request object

	if err != nil {
		panic(err)
	}

	// defer will skip the execution and go to next operation and comes back at the end of the function
	// execution defered until the function terminates
	defer resp.Body.Close() // like that of postpone... literal meaning 


	// Reading the response body
	body, _:= io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)  // comparing the status code from repsonse with statusOK

	// comparing resposne body
	assert.JSONEq(t, `{"message": "login success"}`, string(body))
}

