package controller

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

// handler function
func HomeHandler(w http.ResponseWriter, r *http.Request){ // two parameters // ResponseWriter is of interface type and Request is of pointer type point to some repuest which is of struct type 
	// send some message w for writing 
	_,err := w.Write([]byte("Hello world")) // write into the body
	if err !=nil {
		fmt.Println("error:", err)
	}
}

func ParameterHandler(w http.ResponseWriter, r *http.Request){ // fixed argument in any handler
	para :=mux.Vars(r) // to extract the parameter url // of type map 
	name := para["myname"]
	// fmt.Println(name)
	// fmt.Println(para)

	_, err := w.Write([]byte ("My name is " + name))
	if err != nil {
		os.Exit(1)
	}

}